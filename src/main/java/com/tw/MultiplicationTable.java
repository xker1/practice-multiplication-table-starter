package com.tw;

public class MultiplicationTable {
    public String buildMultiplicationTable(int start, int end) {
        if (isValid(start, end)){
            return generateTable(start, end);
        }else {
            return null;
        }
    }

    public Boolean isValid(int start, int end) {
        return isInRange(start) && isInRange(end) && isStartNotBiggerThanEnd(start, end);
    }

    public Boolean isInRange(int number) {
        return 1 <= number && 1000 >= number;
    }

    public Boolean isStartNotBiggerThanEnd(int start, int end) {
        return start <= end;
    }

    public String generateTable(int start, int end) {
        StringBuilder table = new StringBuilder();
        for (int row = start; row <= end; row++){
            String line = generateLine(start, row);
            table.append(line);
            if (row != end){
                table.append("\r\n");
            }
        }
        return String.format(table.toString());
    }

    public String generateLine(int start, int row) {
        StringBuilder line = new StringBuilder();
        for (int multiplier = start; multiplier <= row; multiplier++){
            String singleExpression = generateSingleExpression(multiplier, row);
            line.append(singleExpression);
            if (multiplier != row){
                line.append("  ");
            }
        }
        return line.toString();
    }

    public String generateSingleExpression(int multiplicand, int multiplier) {
        StringBuilder stringBuilder = new StringBuilder();
        return stringBuilder.append(multiplicand + "*" + multiplier + "=" + multiplicand * multiplier).toString();
    }
}
